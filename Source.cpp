#include "Helper.h"
#include <iostream>
#include "Windows.h"
#include <fstream>


#define BUFSIZE 2048

using namespace std;

string pwd(TCHAR* buffer);
void cd(vector<string> cmd);
void createFile(string name);
bool ListDirectoryContents(const char *sDir);



int main(int argc, TCHAR **argv)
{
	vector<string> cmd;
	string line, cdCheck;
	DWORD out;
	TCHAR  buffer[BUFSIZE] = TEXT("");
	string temp;


	while (true)
	{
		cout << "Enter your command: ";
		getline(cin, temp);
		cmd = Helper::get_words(temp);
		if (cmd[0].compare("pwd") == 0)
			pwd(buffer);
		else if (cmd[0].compare("cd") == 0)
			cd(cmd);
		else if (cmd[0].compare("createfile") == 0)
			createFile(cmd[1]);
		else if (cmd[0].compare("ls") == 0)
			ListDirectoryContents((pwd(buffer)).c_str());


	}
	return 1;
}

string pwd(TCHAR* buffer)
{
	DWORD out;
	out = GetCurrentDirectory(BUFSIZE, buffer);
	if (out == 0)
	{
		printf("GetCurrentDirectory failed (%d)\n", GetLastError());
		system("PAUSE");
	}
	cout << "Current path: " <<  buffer << endl;
	return buffer;
}

void cd(vector<string> cmd)
{
	if (cmd.size() < 1)
	{
		cout << "Invalid argument!" << endl;
	}
	else
	{
		if (!SetCurrentDirectory(cmd[1].c_str()))
			cout << "SetCurrentDirectory failed (" << GetLastError() << ")" << endl;
	}
}

void createFile(string name)
{
	if (!CreateFile((name.c_str()), FILE_READ_DATA, FILE_SHARE_READ,NULL, OPEN_ALWAYS, 0, NULL))
		cout << "CreateFile failed (" << GetLastError() << ")" << endl;

}

bool ListDirectoryContents(const char *sDir)
{
		WIN32_FIND_DATA fdFile;
		HANDLE hFind = NULL;

		char sPath[2048];

		

		if ((hFind = FindFirstFile(sPath, &fdFile)) == INVALID_HANDLE_VALUE)
		{
			printf("Path not found: [%s]\n", sDir);
			return false;
		}

		do
		{
			//Find first file will always return "."
			//    and ".." as the first two directories.
			if (strcmp(fdFile.cFileName, ".") != 0
				&& strcmp(fdFile.cFileName, "..") != 0)
			{
				//Build up our file path using the passed in
				//  [sDir] and the file/foldername we just found:
//				sprintf(sPath, "%s\\%s", sDir, fdFile.cFileName);

				//Is the entity a File or Folder?
				if (fdFile.dwFileAttributes &FILE_ATTRIBUTE_DIRECTORY)
				{
					printf("Directory: %s\n", sPath);
					ListDirectoryContents(sPath); //Recursion, I love it!
				}
				else{
					printf("File: %s\n", sPath);
				}
			}
		} while (FindNextFile(hFind, &fdFile)); //Find the next file.

		FindClose(hFind); //Always, Always, clean things up!

		return true;
}